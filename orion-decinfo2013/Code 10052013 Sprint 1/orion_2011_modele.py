import random
from helper import Helper
import others

class Case():
    def __init__(self,parent,x,y):
        self.parent=parent
        self.x=0
        self.y=0
        self.ressources=None
        self.nomRessource=""
        self.content=None
        self.visible=False
    
    def setCaseRessource(self,Ressource):
        self.ressources=Ressource
        
    def setCaseContent(self,Content):
        self.content=Content
        
    def setVisible(self,boolVisible):
        self.visible=Visible
        
class Etoile(object):
    def __init__(self,parent,id,x,y,terre):
        self.parent=parent
        self.id=id
        self.proprietaire=""
        self.x=x
        self.y=y
        self.taille=random.randrange(100)+10
        self.etoileterre=terre
        self.planetes=[]
        self.creePlanetes()
        self.stations=[]
        self.explored=0
        self.satisfactionAVG=0
        
        
    def assigneRessources(self,Bool):
        if(Bool == True):
            ressources ={   "food" : 10000,
                            "wood" : 10000,
                            "mineral" : 10000,
                            "gas" : 10000
                        }
        else:
            ressources ={   "food" : random.randrange(self.parent.ressourcesSettings["minFood"],self.parent.ressourcesSettings["maxFood"]),
                            "wood" : random.randrange(self.parent.ressourcesSettings["minWood"],self.parent.ressourcesSettings["maxWood"]),
                            "mineral" : random.randrange(self.parent.ressourcesSettings["minMineral"],self.parent.ressourcesSettings["maxMineral"]),
                            "gas" : random.randrange(self.parent.ressourcesSettings["minGas"],self.parent.ressourcesSettings["maxGas"])
                        }
        #print(ressources)
        return ressources
        
    def creePlanetes(self):
        n=random.randrange(3)
        tmin=self.taille*3
        tmax=self.taille*20
        np=random.randrange(10)
            
        for i in range(np):
            dirx=random.randrange(2)-1
            if dirx==0:
                dirx=1
            diry=random.randrange(2)-1
            if diry==0:
                diry=1
            x=random.randrange(tmin,tmax)*dirx
            y=random.randrange(tmin,tmax)*diry
            t=random.randrange(10)+4
            if self.etoileterre and i == np-1:
                e=Planete(self,self.parent.Pid,x,y,t,self.assigneRessources(True),True)
                self.planetes.append(e)
                e.CC= others.CommandCenter(e)
                e.caseList[str(25)+","+str(25)].content="Command Center"
                e.listeBatiment.append(e.CC)
                for i in range(5):
                    e.CC.creerTravailleur()
                
            else:
                self.planetes.append(Planete(self,self.parent.Pid,x,y,t,self.assigneRessources(False),False))
            self.parent.Pid+=1
            
        
class Planete(object):
    def __init__(self,parent,id,x,y,t,ressources, terre):
        self.parent=parent
        self.x=x
        self.y=y
        self.id = id
        self.taille=t
        self.ressources=ressources
        self.terre = terre
        self.caseList = {}
        self.upkeepTotal = 0    
        self.systemeSolaire = ""
        self.nom = ""
        self.listeTravailleur = []
        self.generateCaseList(50, 50)
        self.ressourcesPlacement(self.parent.parent.gameSettings["RPC"])
        self.CC=None
        self.listeBatiment=[]
        
    def ressourcesPlacement(self, RPC):
        n = self.ressources["food"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="food"
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(1000)
                    n = n - 1000
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                
        n = self.ressources["wood"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="wood"
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(RPC)
                    n = n - RPC
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                
        n = self.ressources["mineral"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="mineral"
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(RPC)
                    n = n - RPC
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                
        n = self.ressources["gas"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="gas"
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(RPC)
                    n = n - RPC
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                
    
    def generateCaseList(self,sizeX,sizeY):
        for i in range(sizeX):
            for j in range(sizeY):
                position=str(i)+","+str(j)
                self.caseList[position]=Case(self,i,j)
                
    def initPlaneteTerre(self):
        if self.terre:
            self.ressourcesDisponible["Wood"]=250
            self.ressourcesDisponible["Food"]=250
            self.CC = others.CommandCenter()
            positionX = self.taille/2-self.CC.surface
            positionY = self.taille/2-self.CC.surface
            self.caseList[str(positionX)+","+str(positionY)].content = "Command Center"
            print("Command Center en:"+str(positionX)+","+str(positionY))
            for i in range(self.CC.surface): 
                caseX=positionX+i
                for j in range(self.CC.surface):  
                    caseY=positionY+j
                    self.caseList[str(caseX)+","+str(caseY)].content = "Command Center"
                    print("Command Center en:"+str(caseX)+","+str(caseY))
        
class Civ(object):
    def __init__(self,parent,nom,e,p,couleur):
        self.parent=parent
        self.nom=nom
        self.etoileMere=e
        self.planeteMere=p
        self.couleur=couleur
        self.planetesColonisees=[p]
        self.etoilesVisites=[e]
        self.artefactsDecouverts=[]
        self.flottes=[]
        self.artefacts={"station":[Station(self,Modele.nextId())],
                       "vaisseau":[Vaisseau(self,Modele.nextId(),p.parent)],
                       "batiment":[],
                       "extracteur":[],
                       "balise":[]}
        self.actions={"changeCible":self.changeCible,
                      "message":self.parent.parent.vue.ecrireMessage}
                       
    def changeCible(self,par):
        id,x,y=par
        for i in self.artefacts["vaisseau"]:
            if id==i.id:
                i.changeCible(x,y)
                print("CHANGER VAISSEAU CIBLE")
                
    def prochaineAction(self):
        for i in self.artefacts.keys():
            for j in self.artefacts[i]:
                j.prochaineAction()
                                
    def evalueAction(self):
        for i in self.artefacts.keys():
            for j in self.artefacts[i]:
                x=34*3/4*5+9
        for i in self.artefacts.keys():
            for j in self.artefacts[i]:
                x=34*3/4*5+9

class Station(object):
    def __init__(self,parent,id):
        self.parent=parent
        self.id=id
        self.nom=parent.nom
        dirx=random.randrange(2)-1
        if dirx==0:
            dirx=1
        diry=random.randrange(2)-1
        if diry==0:
            diry=1
        self.x=(random.randrange(10)+10*dirx)+parent.planeteMere.parent.x
        self.y=(random.randrange(10)+10*diry)+parent.planeteMere.parent.y
        print("STATION",self.x,self.y)
        
    def prochaineAction(self):
        pass
  
class Vaisseau(object):
    def __init__(self,parent,id,p,x=20,y=20):
        self.parent=parent
        self.id=id
        self.type=random.choice(["navette","croiseur","cargo"])
        self.nom=parent.nom
        self.x=p.x+x
        self.y=p.y+y
        self.vitesse=5
        self.angle=0
        self.cible=[]
        
    def changeCible(self,x,y):
        self.cible=[x,y]
        self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])
        #print("ANGLE",self.angle,math.degrees(self.angle), self.x,self.y,x,y)
        
    def bougerVaisseau(self):
        if self.cible:
            x,y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)

            #print("CALC2",[x,y])
            d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
            if d<self.vitesse:
                self.cible=[]
                
    def prochaineAction(self):
        #print("DO SOMETING")
        self.bougerVaisseau()
             
class Modele(object):
    id=0
    def nextId():
        Modele.id=Modele.id+1
        return Modele.id
    
    
    def __init__(self,parent):
        self.parent=parent
        self.paramPartie={"x_espace":1300,
                          "y_espace":800,
                          "etoiles":10,
                          "pseudoetoiles":600,
                          "max_planetes":10,
                          "min_minerai":10000,
                          "max_minerai":10000,
                          "min_energie":10000,
                          "max_energie":10000,
                          }
        self.gameSettings={"x_espace":10000,
                           "y_espace":8000,
                           "z_espace":6000,
                           "maxSolarSystems":10,
                           "maxPlanets":100,
                           "artefactCount":10,
                           "relicCount":10,
                           "planetsPerSystem":10,
                           "RPC":2000
                           }
        self.ressourcesSettings={"maxMineral":10000,
                                 "maxFood":10000,
                                 "maxGas":10000,
                                 "maxWood":10000,
                                 "minMineral":1000,
                                 "minFood":1000,
                                 "minGas":1000,
                                 "minWood":1000
                                }
        self.rdseed=0
        self.civs={}
        self.vaisseau=""
        self.vaisseaux=[]
        self.actions=[]
        self.actionsAFaire={}
        self.etoiles=[]
        self.pseudoetoiles=[]
        self.Pid=0
        self.Eid=0
        
    def initPartie(self,listeNomsJoueurs):
        for i in range(self.paramPartie["etoiles"]):
            x=random.randrange(self.paramPartie["x_espace"])
            y=random.randrange(self.paramPartie["y_espace"])
            self.etoiles.append(Etoile(self,self.Eid,x,y,False))
            self.Eid+=1
        
        for i in range(self.paramPartie["pseudoetoiles"]):
            x=random.randrange(self.paramPartie["x_espace"])
            y=random.randrange(self.paramPartie["y_espace"])
            #id=Modele.nextId()
            self.pseudoetoiles.append([x,y])
        couleurs=["red","blue","green","yellow","orange","purple","pink","lightblue"]
        n=0
        for j in listeNomsJoueurs:
            x=random.randrange(self.paramPartie["x_espace"])
            y=random.randrange(self.paramPartie["y_espace"])
            s=1
            while s:
                e=Etoile(self,self.Eid,x,y,True)
                self.etoiles.append(e)
                self.Eid+=1
                if len(e.planetes)>4:
                    s=0
            print("NOM",j)        
            p=e.planetes[random.randrange(len(e.planetes))]
            #self.etoiles.append(e)
            self.civs[j]=Civ(self,j,e,p,couleurs[n])
            n=n+1

        
#    def creerVaisseau(self,):
#        x=random.randrange(self.parent.largeur_espace)
#        y=random.randrange(self.parent.hauteur_espace)
#        random.seed(self.rdseed)
#        self.actions.append(["creerVaisseau",[self.nom,x,y]])
#        self.creerEtoiles()
#        
#    def creerEtoiles(self):
#        for i in range(2000):
#            x=random.randrange(self.parent.largeur_espace)
#            y=random.randrange(self.parent.hauteur_espace)
#            self.etoiles.append(Etoile(self,x,y))
        
    def prochaineAction(self,cadre):
        if cadre in self.actionsAFaire:
            for i in self.actionsAFaire[cadre]:
                #print("ACTIONENCOURS",i)
                if i[1] == "changeCible":
                    self.civs[i[0]].actions[i[1]](i[2])
                elif i[1] == "message":
                    self.civs[i[0]].actions[i[1]](i[2],i[0])
            del self.actionsAFaire[cadre]
            #print("NO1",cadre)
                
        for i in self.civs.keys():
            self.civs[i].prochaineAction()
            
        for i in self.civs.keys():
            self.civs[i].evalueAction()