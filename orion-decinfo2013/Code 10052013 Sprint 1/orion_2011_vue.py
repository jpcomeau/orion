# -*- coding: ISO-8859-1 -*-
from tkinter import *
from tkinter.font import *
import tkinter.simpledialog as sd
import tkinter.messagebox as mb
import random
import math
from helper import *
from PIL import Image, ImageTk

class Buttonjm(Button):
    def __init__(self,parent,**kw):
        f=Font(size=7,slant="italic",weight="bold")
        kw["font"]=f
        kw["fg"]="orange"
        kw["bg"]="grey25"
        kw["relief"]="groove"
        Button.__init__(self,parent,**kw)
class Labeljm(Label):
    def __init__(self,parent,**kw):
        f=Font(size=7)
        kw["font"]=f
        kw["fg"]="orange"
        kw["bg"]="grey25"
        Label.__init__(self,parent,**kw)

class VueCosmos(object):
    perspective=["cosmos","espace","systeme","planete"]
    
    def __init__(self,parent,x,y):
        self.parent=parent
        
    def initCosmos(self,evt=""):
        self.canevasCosmos.delete(ALL)
        divx=self.x_espace/self.canevasCosmos.winfo_width()
        divy=self.y_espace/self.canevasCosmos.winfo_height()
        n=1
        for i in self.partie.etoiles:
            x=i.x/divx
            y=i.y/divy
            self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill="yellow",tags=("fond","etoile",i.id))
        self.afficheCosmos()
    
            
    def creeCadreCosmos(self):
        self.cadreCosmos=Frame(self.cadrePartie)
        self.cadreCosmos.rowconfigure(0,weight=1)
        self.cadreCosmos.columnconfigure(0,weight=1)

        sy=Scrollbar(self.cadreCosmos,orient=VERTICAL)
        sx=Scrollbar(self.cadreCosmos,orient=HORIZONTAL)
        self.canevasCosmos=Canvas(self.cadreCosmos,width=self.x,height=self.y,
                            xscrollcommand=sx.set,yscrollcommand=sy.set,bd=0,
                            scrollregion=(0,0,self.x,self.y),bg="grey15")
        sx.config(command=self.canevasCosmos.xview)
        sy.config(command=self.canevasCosmos.yview)
        
        self.canevasCosmos.grid(row=0,column=0,sticky=N+S+W+E)
        sy.grid(row=0,column=1,sticky=N+S)
        sx.grid(row=1,column=0,sticky=W+E)
        self.canevasCosmos.bind("<Button-1>",self.selectObject)
        self.canevasCosmos.bind("<Configure>",self.initCosmos)
        
    def afficheCosmos(self):
        self.canevasCosmos.delete("miseajour")
        divx=self.x_espace/self.canevasCosmos.winfo_width()
        divy=self.y_espace/self.canevasCosmos.winfo_height()
        n=1
        j=self.partie.civs[self.moi]
        i=j.etoileMere
        x=i.x/divx
        y=i.y/divy
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill=j.couleur,tags=("miseajour","etoile",i.id,j.nom,"etoilemere"))
        n=3
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, outline=j.couleur,tags=("miseajour","etoile",i.id,j.nom,"etoilemere"))
    
        
class VueEspace(object):
    perspective=["cosmos","espace","systeme","planete"]
    
    def __init__(self,parent,x,y,x_espace,y_espace):
        self.parent=parent
    
    def afficheArtefact(self):
        self.canevasEspace.delete("artefact")
        for i in self.partie.civs.keys():
            n=12
            coul=self.partie.civs[i].couleur
            for j in self.partie.civs[i].artefacts["vaisseau"]:
                x=j.x*self.zoomFactor
                y=j.y*self.zoomFactor
                if j.type=="navette":
                    x1,y1=Helper.getAngledPoint(j.angle,3,x,y)
                    x2,y2=Helper.getAngledPoint(j.angle,8,x1,y1)
                    x3,y3=Helper.getAngledPoint(j.angle,4,x2,y2)
                    self.canevasEspace.create_line(x,y,x1,y1,
                                                   width=3,
                                                   fill="yellow",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x1,y1,x2,y2,
                                                   width=4,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x2,y2,x3,y3,
                                                   width=2,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                elif j.type=="cargo":
                    x1,y1=Helper.getAngledPoint(j.angle,8,x,y)
                    x2,y2=Helper.getAngledPoint(j.angle,8,x1,y1)
                    x3,y3=Helper.getAngledPoint(j.angle,8,x2,y2)
                    self.canevasEspace.create_line(x,y,x1,y1,
                                                   width=3,
                                                   fill="yellow",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_oval(x1-5,y1-5,x1+5,y1+5,
                                                   width=1,outline="white",
                                                   fill="white",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x2,y2,x3,y3,
                                                   width=2,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                elif j.type=="croiseur":
                    x1,y1=Helper.getAngledPoint(j.angle,4,x,y)
                    x2,y2=Helper.getAngledPoint(j.angle,18,x1,y1)
                    x3,y3=Helper.getAngledPoint(j.angle,9,x2,y2)
                    self.canevasEspace.create_line(x,y,x1,y1,
                                                   width=4,
                                                   fill="yellow",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x1,y1,x2,y2,
                                                   width=12,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x2,y2,x3,y3,
                                                   width=3,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                
            
            n=5*self.zoomFactor
            for j in self.partie.civs[i].artefacts["station"]:
                x=j.x*self.zoomFactor
                y=j.y*self.zoomFactor
                self.canevasEspace.create_rectangle(x-n,y-n,x+n,y+n,outline=coul,fill=coul,tags=("artefact",i,j.id,"station"))
                self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,outline=coul,fill="lightblue",tags=("artefact",i,j.id,"station"))
        self.afficheSelection()
        
    
        
    def afficheEtoiles(self):
        self.canevasEspace.delete("etoiles")
        self.canevasEspace.delete("pseudoetoiles")
        n=3
        for i in self.partie.etoiles:
            n=i.taille/20*self.zoomFactor
            x=i.x*self.zoomFactor
            y=i.y*self.zoomFactor
            self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,fill="yellow",tags=("etoiles",i.id))
        e=self.partie.civs[self.parent.nom].planeteMere.parent
        n=4*self.zoomFactor
        x=e.x*self.zoomFactor
        y=e.y*self.zoomFactor
        self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,width=2,outline="red",tags=("etoiles",e.id,self.parent.nom,"etoilemere"))
        n=1
        for i in self.partie.pseudoetoiles:
            x=i[0]*self.zoomFactor
            y=i[1]*self.zoomFactor
            self.canevasEspace.create_oval(x,y,x+n,y+n,fill="white",tags=("pseudoetoiles",))
        
             
    def zoomInEspace(self):
        self.zoomFactor=self.zoomFactor*2
        self.x_espace=self.x_espace*2
        self.y_espace=self.y_espace*2
        self.canevasEspace.config(scrollregion=(0,0,self.x_espace,self.y_espace))
        self.canevasEspace.config(xscrollcommand=self.sxespace.set)
        self.canevasEspace.config(yscrollcommand=self.syespace.set)
        self.initEspace()      
          
    def zoomOutEspace(self):
        self.zoomFactor=self.zoomFactor/2
        self.x_espace=self.x_espace/2
        self.y_espace=self.y_espace/2
        self.canevasEspace.config(scrollregion=(0,0,self.x_espace,self.y_espace))
        self.canevasEspace.config(xscrollcommand=self.sxespace.set)
        self.canevasEspace.config(yscrollcommand=self.syespace.set)
        self.initEspace()
        
    def creeCadreEspace(self):
        self.cadreEspace=Frame(self.cadrePartie)
        self.cadreEspace.rowconfigure(0,weight=1)
        self.cadreEspace.columnconfigure(0,weight=1)

        self.syespace=Scrollbar(self.cadreEspace,orient=VERTICAL)
        self.sxespace=Scrollbar(self.cadreEspace,orient=HORIZONTAL)
        self.canevasEspace=Canvas(self.cadreEspace,width=self.x,height=self.y,
                            xscrollcommand=self.sxespace.set,yscrollcommand=self.syespace.set,bd=0,
                            scrollregion=(0,0,self.x_espace,self.y_espace),bg="grey10")
        self.sxespace.config(command=self.canevasEspace.xview)
        self.syespace.config(command=self.canevasEspace.yview)
        
        self.canevasEspace.bind("<Button-1>",self.selectObject)
        
        self.canevasEspace.grid(row=0,column=0,sticky=N+S+W+E)
        self.syespace.grid(row=0,column=1,sticky=N+S)
        self.sxespace.grid(row=1,column=0,sticky=W+E)
        
             
    def initEspace(self):
        self.afficheEtoiles()
        self.afficheArtefact()
    
       
        
class VueSysteme(object):
    perspective=["cosmos","espace","systeme","planete"]
    
    def __init__(self,parent,x,y,x_espace,y_espace):
        self.parent=parent
        self.zoomFactor=1
        
    def creeCadreSysteme(self):
        self.cadreSysteme=Frame(self.cadrePartie)
        self.cadreSysteme.rowconfigure(0,weight=1)
        self.cadreSysteme.columnconfigure(0,weight=1)

        sy=Scrollbar(self.cadreSysteme,orient=VERTICAL)
        sx=Scrollbar(self.cadreSysteme,orient=HORIZONTAL)
        self.canevasSysteme=Canvas(self.cadreSysteme,width=self.x,height=self.y,
                            xscrollcommand=sx.set,yscrollcommand=sy.set,bd=0,
                            bg="grey15")
        sx.config(command=self.canevasSysteme.xview)
        sy.config(command=self.canevasSysteme.yview)
        
        self.canevasSysteme.grid(row=0,column=0,sticky=N+S+W+E)
        sy.grid(row=0,column=1,sticky=N+S)
        sx.grid(row=1,column=0,sticky=W+E)
        self.canevasSysteme.bind("<Configure>",self.initSysteme)
            
    def exploreSysteme(self):
        pass
        
    def initSysteme(self,evt=""):
        if self.etoileselection:
            self.afficheSysteme(self.etoileselection)
       
class VuePlanete(object):
    perspective=["cosmos","espace","systeme","planete"]
    
    def __init__(self,parent,x,y,x_espace,y_espace):
        self.parent=parent
    
    def colonisePlanete(self):
        pass
    
    def initPlanete(self,evt=""):
        self.affichePlanete()
        
    def creeCadrePlanete(self):
        self.cadrePlanete=Frame(self.cadrePartie)
        self.cadrePlanete.rowconfigure(0,weight=1)
        self.cadrePlanete.columnconfigure(0,weight=1)

        sy=Scrollbar(self.cadrePlanete,orient=VERTICAL)
        sx=Scrollbar(self.cadrePlanete,orient=HORIZONTAL)
        self.canevasPlanete=Canvas(self.cadrePlanete,width=self.x,height=self.y,
                            xscrollcommand=sx.set,yscrollcommand=sy.set,bd=0,
                            scrollregion=(0,0,self.x,self.y),bg="grey15")
        sx.config(command=self.canevasPlanete.xview)
        sy.config(command=self.canevasPlanete.yview)
        
        self.canevasPlanete.grid(row=0,column=0,sticky=N+S+W+E)
        sy.grid(row=0,column=1,sticky=N+S)
        sx.grid(row=1,column=0,sticky=W+E)
        self.canevasPlanete.bind("<Button-1>",self.selectObject)
        self.canevasPlanete.bind("<Button-3>",self.deplacer)
          
class Vue(object):
    perspective=["cosmos","espace","systeme","planete"]
    
    def __init__(self,parent,x,y,x_espace,y_espace):
        self.parent=parent
        self.zoomFactor=1
        self.modele=self.parent.modele
        self.selection=[]
        self.mesphotos=[]
        self.etoileselection=0
        self.planeteselection=0
        self.travailleurselection=0
        self.con=0
        self.x=x
        self.y=y
        self.x_espace=x_espace
        self.y_espace=y_espace
        #self.planetimg=Photo
        self.root=Tk()
        self.root.protocol('WM_DELETE_WINDOW', self.intercepteFermeture)
        self.cadreActif=0
        self.perspective=""
        self.perspectives={}
        self.currentStar = None
        self.currentPlanet = None
        self.perspectiveCourante=None
        self.canevasCourant=None
        self.selections=[]
        self.creeCadres()
        #self.ecranPartie(x,y,x_espace,y_espace)
        self.placeCadre(self.cadreConnection)

    def creeCadres(self):
        self.creeCadreConnection()
        self.creeCadreAttente()
        self.creeCadrePartie()
        
    def creeCadrePartie(self):
        self.cadrePartie=Frame(self.root)
        self.cadrePartie.rowconfigure(1,weight=1)
        self.cadrePartie.columnconfigure(0,weight=1)
        
        self.creeCadreMenuPartie()
        self.creeCadreInfoCiv()
        self.creeCadreCosmos()
        self.creeCadreEspace()
        self.creeCadreSysteme()
        self.creeCadrePlanete()
        self.perspectives={"Cosmos":self.cadreCosmos,
                           "Espace":self.cadreEspace,
                           "Systeme":self.cadreSysteme,
                           "Planete":self.cadrePlanete}
        self.canevas={"Cosmos":self.canevasCosmos,
                           "Espace":self.canevasEspace,
                           "Systeme":self.canevasSysteme,
                           "Planete":self.canevasPlanete}
        self.creeCadreTraiteChat()
        self.creeCadreNiveauVue()

    def creeCadreMenuPartie(self):
        self.cadreMenuPartie=Frame(self.cadrePartie,height=40,bg="grey25")
        self.cadreMenuPartie.grid_propagate(0)
        
        trouveB=Buttonjm(self.cadreMenuPartie,text="TrouvePartie planete",command=self.centrerPlanete)
        trouveB.grid(row=0,column=0,sticky=N)
        zoomin=Buttonjm(self.cadreMenuPartie,text="ZoomIn",relief=GROOVE,command=self.zoomInEspace)
        zoomin.grid(row=0,column=1,sticky=N)
        zoomout=Buttonjm(self.cadreMenuPartie,text="ZoomOut",relief=GROOVE,command=self.zoomOutEspace)
        zoomout.grid(row=0,column=2,sticky=N)
        
        self.labelBouffe = Labeljm(self.cadreMenuPartie,text="Bouffe : 0000",padx=10)
        self.labelBouffe.grid(row=0,column=3)
        e = Font(size=10)
        self.labelBois = Labeljm(self.cadreMenuPartie,text="Bois : 0000",padx=10)
        self.labelBois.grid(row=0,column=4)
        e = Font(size=10)
        self.labelMetal = Labeljm(self.cadreMenuPartie,text="M�tal : 0000",padx=10)
        self.labelMetal.grid(row=0,column=5)
        e = Font(size=10)
        self.labelScience = Labeljm(self.cadreMenuPartie,text="Science : 0000",padx=10)
        self.labelScience.grid(row=0,column=6)
        e = Font(size=10)
        self.labelSatisfaction = Labeljm(self.cadreMenuPartie,text="Satisfaction : 000%",padx=10)
        self.labelSatisfaction.grid(row=0,column=7)
        e = Font(size=10)
        
        self.buttonTree = Buttonjm(self.cadreMenuPartie,text="Arbre de technologies",padx=5)
        self.buttonTree.grid(row=0,column=8)
        self.labelTime = Labeljm(self.cadreMenuPartie,text="Temps : 00000 ans")
        self.labelTime.grid(row=0,column=9)
        
    def zoomInEspace(self):
        self.zoomFactor=self.zoomFactor*2
        self.x_espace=self.x_espace*2
        self.y_espace=self.y_espace*2
        self.canevasEspace.config(scrollregion=(0,0,self.x_espace,self.y_espace))
        self.canevasEspace.config(xscrollcommand=self.sxespace.set)
        self.canevasEspace.config(yscrollcommand=self.syespace.set)
        self.initEspace()      
          
    def zoomOutEspace(self):
        self.zoomFactor=self.zoomFactor/2
        self.x_espace=self.x_espace/2
        self.y_espace=self.y_espace/2
        self.canevasEspace.config(scrollregion=(0,0,self.x_espace,self.y_espace))
        self.canevasEspace.config(xscrollcommand=self.sxespace.set)
        self.canevasEspace.config(yscrollcommand=self.syespace.set)
        self.initEspace()
        
    def creeCadreInfoCiv(self):
        self.cadreInfoCiv=Frame(self.cadrePartie,height=40,width=100,bg="grey25")
        self.cadreInfoCiv.grid_propagate(0)
        
        
    def creeCadreCosmos(self):
        self.cadreCosmos=Frame(self.cadrePartie)
        self.cadreCosmos.rowconfigure(0,weight=1)
        self.cadreCosmos.columnconfigure(0,weight=1)

        sy=Scrollbar(self.cadreCosmos,orient=VERTICAL)
        sx=Scrollbar(self.cadreCosmos,orient=HORIZONTAL)
        self.canevasCosmos=Canvas(self.cadreCosmos,width=self.x,height=self.y,
                            xscrollcommand=sx.set,yscrollcommand=sy.set,bd=0,
                            scrollregion=(0,0,self.x,self.y),bg="grey15")
        sx.config(command=self.canevasCosmos.xview)
        sy.config(command=self.canevasCosmos.yview)
        
        self.canevasCosmos.grid(row=0,column=0,sticky=N+S+W+E)
        sy.grid(row=0,column=1,sticky=N+S)
        sx.grid(row=1,column=0,sticky=W+E)
        self.canevasCosmos.bind("<Button-1>",self.selectObject)
        self.canevasCosmos.bind("<Configure>",self.initCosmos)
    
    def selectObject(self,evt):
        t=self.canevasCourant.gettags(CURRENT)
        if t:
            print("CHOSE",t)
            print(t[0])
            if t[0]=="etoiles":
                for i in self.modele.etoiles:
                    if i.id==int(t[1]):
                        if i == self.etoileselection:
                            self.etoileselection=0
                        else:
                            self.etoileselection=i
                        self.afficheSelection()
                        break   
                else:
                    print("etoile pas egale")
                    
            elif t[0]=="planetes":
                for i in self.modele.etoiles[self.etoileselection.id].planetes:
                    if i.id==int(t[1]):
                        if i == self.planeteselection:
                            self.planeteselection=None
                        else:
                            self.planeteselection=i
                        self.afficheSelection()
                        break
                    else:
                        print("pas egale") 
            
            elif t[0]=="travailleur":
                for i in self.modele.etoiles[self.etoileselection.id].planetes:
                    if i.id== self.planeteselection.id:
                        for j in i.listeTravailleur:
                            if j.id == int(t[1]):
                                print(j.id)
                                if i == self.travailleurselection:
                                    self.travailleurselection=None
                                    self.cadreActions.pack_forget()
                                else:
                                    self.travailleurselection=j
                                    self.cadreTravailleur()
                                self.afficheSelection()
                                break
                            else:
                                print("pas egale")
                        
            elif t[1]==self.parent.nom:
                if "vaisseau" in t:
                    for i in self.modele.civs[t[1]].artefacts["vaisseau"]:
                        if i.id==int(t[2]):
                            if i in self.selection:
                                self.selection.remove(i)
                            else:
                                self.selection.append(i)
                            self.afficheSelection()
                            break
                        #else:
                            #print("PAS BON VAISSEAU")
                elif "station" in t:
                    for i in self.modele.civs[t[1]].artefacts["station"]:
                        if i.id==int(t[2]):
                            pass
            else:
                print("PAS MOI")
                              
        elif self.selection:
            #input(self.selection)
            print("SELECTION",self.selection)
            self.changeCible(self.selection,evt)
        else:
            self.selection=[]
        #self.artefacttype1.config(text=t)
        
    def creeCadreEspace(self):
        self.cadreEspace=Frame(self.cadrePartie)
        self.cadreEspace.rowconfigure(0,weight=1)
        self.cadreEspace.columnconfigure(0,weight=1)

        self.syespace=Scrollbar(self.cadreEspace,orient=VERTICAL)
        self.sxespace=Scrollbar(self.cadreEspace,orient=HORIZONTAL)
        self.canevasEspace=Canvas(self.cadreEspace,width=self.x,height=self.y,
                            xscrollcommand=self.sxespace.set,yscrollcommand=self.syespace.set,bd=0,
                            scrollregion=(0,0,self.x_espace,self.y_espace),bg="grey10")
        self.sxespace.config(command=self.canevasEspace.xview)
        self.syespace.config(command=self.canevasEspace.yview)
        
        self.canevasEspace.bind("<Button-1>",self.selectObject)
        
        self.canevasEspace.grid(row=0,column=0,sticky=N+S+W+E)
        self.syespace.grid(row=0,column=1,sticky=N+S)
        self.sxespace.grid(row=1,column=0,sticky=W+E)
        
    def creeCadreSysteme(self):
        self.cadreSysteme=Frame(self.cadrePartie)
        self.cadreSysteme.rowconfigure(0,weight=1)
        self.cadreSysteme.columnconfigure(0,weight=1)

        sy=Scrollbar(self.cadreSysteme,orient=VERTICAL)
        sx=Scrollbar(self.cadreSysteme,orient=HORIZONTAL)
        self.canevasSysteme=Canvas(self.cadreSysteme,width=self.x,height=self.y,
                            xscrollcommand=sx.set,yscrollcommand=sy.set,bd=0,
                            bg="grey15")
        sx.config(command=self.canevasSysteme.xview)
        sy.config(command=self.canevasSysteme.yview)
        
        self.canevasSysteme.bind("<Button-1>",self.selectObject)
        
        self.canevasSysteme.grid(row=0,column=0,sticky=N+S+W+E)
        sy.grid(row=0,column=1,sticky=N+S)
        sx.grid(row=1,column=0,sticky=W+E)
        self.canevasSysteme.bind("<Configure>",self.initSysteme)
        
    def initSysteme(self,evt=""):
        if self.etoileselection:
            self.afficheSysteme(self.etoileselection)
    
    def initPlanete(self,evt=""):
        self.affichePlanete()
    
    def parsePosition(self,str): 
        nums = [int(n) for n in str.split(',')]
        return nums[0] , nums[1]
           
    def creeCadrePlanete(self):
        self.cadrePlanete=Frame(self.cadrePartie)
        self.cadrePlanete.rowconfigure(0,weight=1)
        self.cadrePlanete.columnconfigure(0,weight=1)

        self.sy=Scrollbar(self.cadrePlanete,orient=VERTICAL)
        self.sx=Scrollbar(self.cadrePlanete,orient=HORIZONTAL)
        self.canevasPlanete=Canvas(self.cadrePlanete,width=self.x,height=self.y,
                            xscrollcommand=self.sx.set,yscrollcommand=self.sy.set,bd=0,
                            scrollregion=(0,0,50*50,50*50),bg="darkgreen")
        self.canevasPlanete.bind("<Button-1>",self.selectObject)
        self.canevasPlanete.bind("<Button-2>",self.construire)
        self.canevasPlanete.bind("<Button-3>",self.deplacer)
        self.sx.config(command=self.canevasPlanete.xview)
        self.sy.config(command=self.canevasPlanete.yview)
        
        self.canevasPlanete.grid(row=0,column=0,sticky=N+S+W+E)
        self.sy.grid(row=0,column=1,sticky=N+S)
        self.sx.grid(row=1,column=0,sticky=W+E)
        
    def creeCadreTraiteChat(self):
        self.cadreTraiteChat=Frame(self.cadrePartie,width=300,bg="grey25")
        self.cadreTraiteChat.grid_propagate(0)
        self.cadreTraite = Frame(self.cadreTraiteChat,width=300,height=100,bg="grey25")
        self.cadreTraite.grid(column=0,row=0)
        self.cadreChatLog = Frame(self.cadreTraiteChat,bd=5,relief=GROOVE,width=300,height=600,bg="grey25")
        self.chatText = Text(self.cadreChatLog,width=29,bg="white")
        #self.chatScroll = Scrollbar(self.cadreChatLog)
        #self.chatScroll.pack(side=RIGHT, fill=Y)
        self.chatText.config(state=DISABLED)
        self.chatText.grid(column=0,row=0)
        #self.chatScroll.config(command=self.chatText.yview)
        #self.chatText.config(yscrollcommand=self.chatScroll.set)
        self.cadreChatLog.grid(column=0,row=1)
        self.inputText = Entry(self.cadreTraiteChat,width=37)
        self.inputText.bind("<Return>",self.envoyerMessage)
        self.inputText.grid(column=0,row=2)
        
    def creeCadreNiveauVue(self):
        self.cadreNiveauVue=Frame(self.cadrePartie,height=100,bg="grey25")
        self.cadreNiveauVue.grid_propagate(0)
        self.cadreVue = Frame(self.cadreNiveauVue,width=100,height=100,bg="grey25")
        self.cadreVue.grid(column=0,row=0)
        bcosmos=Buttonjm(self.cadreVue,text="Cosmos")
        bcosmos.grid(column=0,row=0,sticky=N)
        bcosmos.bind("<Button>",self.placeCadreNiveau)
        bespace=Buttonjm(self.cadreVue,text="Espace")
        bespace.grid(column=0,row=1,sticky=N)
        bespace.bind("<Button>",self.placeCadreNiveau)
        bsysteme=Buttonjm(self.cadreVue,text="Systeme")
        bsysteme.grid(column=0,row=2,sticky=N)
        bsysteme.bind("<Button>",self.placeCadreNiveau)
        bplanete=Buttonjm(self.cadreVue,text="Planete")
        bplanete.grid(column=0,row=3,sticky=N)
        bplanete.bind("<Button>",self.placeCadreNiveau)
        
    def cadreTravailleur(self):
        self.cadreActions = Frame(self.cadreNiveauVue,width=400,height=100,bg="grey25")
        self.cadreActions.grid(column=3,row=0,sticky=E)
        self.cadreActions.grid_propagate(0)
        bferme=Buttonjm(self.cadreActions,text="Ferme")
        bferme.grid(column=0,row=0,sticky=N)
        bferme.bind("<Button>",self.construire)
        bscierie=Buttonjm(self.cadreActions,text="Scierie")
        bscierie.grid(column=0,row=1,sticky=N)
        bscierie.bind("<Button>",self.construire)
        bmine=Buttonjm(self.cadreActions,text="Mine")
        bmine.grid(column=0,row=2,sticky=N)
        bmine.bind("<Button>",self.construire)
        
    def placeCadreNiveau(self,evt):   
        t=evt.widget.cget("text")
        print("PRESPECTIVE",t)
        self.placePerspective(t)
        
    def intercepteFermeture(self):
        print("Je me ferme")
        self.parent.jeQuitte()
        self.root.destroy()
        
    def afficheAttente(self):
        self.placeCadre(self.cadreAttente)
        
    def placeCadre(self,c):
        if self.cadreActif:
            self.cadreActif.pack_forget()
        self.cadreActif=c
        self.cadreActif.pack(expand=1,fill=BOTH)
 
    def creeCadreConnection(self):
        self.cadreConnection=Frame(self.root)
        cadreMenu=Frame(self.cadreConnection)

        Nom=Labeljm(cadreMenu,text="Votre NOM svp->")
        self.nomjoueur=Entry(cadreMenu)
        self.nomjoueur.insert(0,"jmd_"+str(random.randrange(100)))
        Nom.grid(column=0,row=0)
        self.nomjoueur.grid(column=1,row=0)
        
        
        lcree=Labeljm(cadreMenu,text="Pour cr�er un serveur � l'adresse inscrite")
        lconnect=Labeljm(cadreMenu,text="Pour vous connecter � un serveur")
        lcree.grid(column=0,row=1)
        lconnect.grid(column=1,row=1)
        
        lip=Labeljm(cadreMenu,text=self.parent.monip)
        self.autreip=Entry(cadreMenu)
        self.autreip.insert(0,self.parent.monip)
        lip.grid(column=0,row=2)
        self.autreip.grid(column=1,row=2)
        
        creerB=Buttonjm(cadreMenu,text="Creer un serveur",command=self.creerServeur)
        connecterB=Buttonjm(cadreMenu,text="Connecter a un serveur",command=self.connecterServeur)
        creerB.grid(column=0,row=3)
        connecterB.grid(column=1,row=3)

        self.galax=PhotoImage(file="galaxie.gif")
        galaxl=Labeljm(self.cadreConnection,image=self.galax)
        galaxl.pack()
        cadreMenu.pack()
        
    def creeCadreAttente(self):
        self.cadreAttente=Frame(self.root)
        cadreMenu=Frame(self.cadreAttente)
        self.listeJoueurs=Listbox(cadreMenu)
        self.demarreB=Buttonjm(cadreMenu,text="Demarre partie",state=DISABLED,command=self.parent.demarrePartie)
        self.demarreB.grid(column=0,row=1)
        self.listeJoueurs.grid(column=0,row=0)
        cadreMenu.pack(side=LEFT)
        self.galax2=PhotoImage(file="galaxie.gif")
        galax=Labeljm(self.cadreAttente,image=self.galax2)
        galax.pack(side=RIGHT)
        
    def afficheListeJoueurs(self,liste):
        self.listeJoueurs.delete(0,END)
        for i in liste:
            self.listeJoueurs.insert(END,i)
        
    def exploreSysteme(self):
        pass
    def colonisePlanete(self):
        pass
        
    def initPartie(self,modele):
        self.partie=modele
        self.moi=modele.parent.nom
        self.initCosmos()
        self.initEspace()
        
        self.cadreMenuPartie.grid(column=0,row=0,columnspan=2,sticky=W+E)
        #self.cadreInfoCiv.grid(column=1,row=0,sticky=W+E+S+N)
        self.cadreTraiteChat.grid(column=1,row=1,rowspan=2,sticky=W+E+S+N)
        self.cadreNiveauVue.grid(column=0,row=2,sticky=W+E+S+N)
        

#        self.perspectiveCourante=self.perspectives["Cosmos"]
#        self.placePerspective("Cosmos")
        self.perspectiveCourante=self.perspectives["Espace"]
        self.placePerspective("Espace")
        
        self.placeCadre(self.cadrePartie)
        
    def placePerspective(self,p="Cosmos"):
        self.perspectiveCourante.grid_forget()
        if p=="Systeme":
            self.initSysteme()
        elif p=="Planete":
            self.initPlanete()
        self.perspectiveCourante=self.perspectives[p]
        self.perspectiveCourante.grid(column=0,row=1,sticky=W+E+S+N)
        self.canevasCourant=self.canevas [p]
        
    def initCosmos(self,evt=""):
        self.canevasCosmos.delete(ALL)
        divx=self.x_espace/self.canevasCosmos.winfo_width()
        divy=self.y_espace/self.canevasCosmos.winfo_height()
        n=1
        for i in self.partie.etoiles:
            x=i.x/divx
            y=i.y/divy
            self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill="yellow",tags=("fond","etoile",i.id))
        self.afficheCosmos()
        
    def afficheCosmos(self):
        self.canevasCosmos.delete("miseajour")
        divx=self.x_espace/self.canevasCosmos.winfo_width()
        divy=self.y_espace/self.canevasCosmos.winfo_height()
        n=1
        j=self.partie.civs[self.moi]
        i=j.etoileMere
        x=i.x/divx
        y=i.y/divy
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill=j.couleur,tags=("miseajour","etoile",i.id,j.nom,"etoilemere"))
        n=3
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, outline=j.couleur,tags=("miseajour","etoile",i.id,j.nom,"etoilemere"))
            
    def afficheSysteme(self,i):
        self.canevasSysteme.delete(ALL)
        divx=self.canevasSysteme.winfo_width()/2
        divy=self.canevasSysteme.winfo_height()/2
        maxx=divx*2
        maxy=divy*2
        for j in i.planetes:
            if abs(j.x)>maxx:
                maxx=abs(j.x)
            if abs(j.y)>maxy:
                maxy=abs(j.y)
        rapx=divx/maxx*0.95
        rapy=divy/maxy*0.95
        if rapx>rapy:
            rap=rapy
        else:
            rap=rapx
        
        
#        image = Image.open("./image/callisto120.gif")
#        image.save("./image/callisto120toto.gif")
#        transparenci = image.info['transparency'] 
       
            #===================================================================
            # print("N2",n)
            # im=image.resize((int(n),int(n)), Image.ANTIALIAS)
            # print("TRANSPARENCE",transparenci)
            # im.save('./image/callisto120_reduit.gif')#,transparency=str(transparenci))
            # im2 = Image.open("./image/callisto120_reduit.gif")
            # self.maphoto = ImageTk.PhotoImage(im2)
            # 
            # self.mesphotos.append(self.maphoto)
            #===================================================================
        #
                
            #self.canevasSysteme.create_image(divx+x,divy+y,image=self.maphoto)
        n=1
        for l in range(random.randrange(200)+50):
            x=random.randrange(divx*2)
            y=random.randrange(divy*2)
            self.canevasSysteme.create_oval(x,y,x+n,y+n,fill="white",tags=("pseudoetoiles",))
            
        n=i.taille*rap
        self.canevasSysteme.create_oval(divx-n,divy-n,divx+n,divy+n,fill="gold")
            
        for j in i.planetes:
            n=j.taille*3*rap
            x=j.x*rapx
            y=j.y*rapy
            print("AfficheSysteme",j.id,divx+x-n,divy+y-n,divx+x+n,divy+y+n)
            if j.terre:
                self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="grey50",fill="red",tags=("planetes",j.id))
            else:
                self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="grey50",fill="aquamarine",tags=("planetes",j.id))
        
    
        #self.canevasSysteme
          
    def affichePlanete(self):
        self.canevasPlanete.delete(ALL)
        for p in self.modele.etoiles[self.etoileselection.id].planetes:
            if p.id == self.planeteselection.id:
                for i in p.caseList.keys():
                    x,y = self.parsePosition(i)
                    x = x*50
                    y = y*50
                    if p.caseList[i].nomRessource == "food":
                        self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="red",tags=("food"))
                    elif p.caseList[i].nomRessource == "wood":
                        self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="purple",tags=("wood"))
                    elif p.caseList[i].nomRessource == "mineral":
                        self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="grey",tags=("mineral"))
                    elif p.caseList[i].nomRessource == "gas":
                        self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="white",tags=("gas")) 
                    elif p.caseList[i].content == "Command Center":
                        self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="gold",tags=("commandcenter"))
               
                for i in p.listeTravailleur:
                    self.canevasPlanete.create_oval(i.x-5,i.y-5,i.x+5,i.y+5,fill="cyan",tags=("travailleur",i.id))
                     
    def initEspace(self):
        self.afficheEtoiles()
        self.afficheArtefact()
    
    def afficheArtefact(self):
        self.canevasEspace.delete("artefact")
        for i in self.partie.civs.keys():
            n=12
            coul=self.partie.civs[i].couleur
            for j in self.partie.civs[i].artefacts["vaisseau"]:
                x=j.x*self.zoomFactor
                y=j.y*self.zoomFactor
                if j.type=="navette":
                    x1,y1=Helper.getAngledPoint(j.angle,3,x,y)
                    x2,y2=Helper.getAngledPoint(j.angle,8,x1,y1)
                    x3,y3=Helper.getAngledPoint(j.angle,4,x2,y2)
                    self.canevasEspace.create_line(x,y,x1,y1,
                                                   width=3,
                                                   fill="yellow",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x1,y1,x2,y2,
                                                   width=4,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x2,y2,x3,y3,
                                                   width=2,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                elif j.type=="cargo":
                    x1,y1=Helper.getAngledPoint(j.angle,8,x,y)
                    x2,y2=Helper.getAngledPoint(j.angle,8,x1,y1)
                    x3,y3=Helper.getAngledPoint(j.angle,8,x2,y2)
                    self.canevasEspace.create_line(x,y,x1,y1,
                                                   width=3,
                                                   fill="yellow",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_oval(x1-5,y1-5,x1+5,y1+5,
                                                   width=1,outline="white",
                                                   fill="white",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x2,y2,x3,y3,
                                                   width=2,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                elif j.type=="croiseur":
                    x1,y1=Helper.getAngledPoint(j.angle,4,x,y)
                    x2,y2=Helper.getAngledPoint(j.angle,18,x1,y1)
                    x3,y3=Helper.getAngledPoint(j.angle,9,x2,y2)
                    self.canevasEspace.create_line(x,y,x1,y1,
                                                   width=4,
                                                   fill="yellow",tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x1,y1,x2,y2,
                                                   width=12,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                    self.canevasEspace.create_line(x2,y2,x3,y3,
                                                   width=3,
                                                   fill=coul,tags=("artefact",i,j.id,"vaisseau"))
                
            
            n=5*self.zoomFactor
            for j in self.partie.civs[i].artefacts["station"]:
                x=j.x*self.zoomFactor
                y=j.y*self.zoomFactor
                self.canevasEspace.create_rectangle(x-n,y-n,x+n,y+n,outline=coul,fill=coul,tags=("artefact",i,j.id,"station"))
                self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,outline=coul,fill="lightblue",tags=("artefact",i,j.id,"station"))
        #self.afficheSelection()
        
    def afficheSelection(self):
        self.canevasEspace.delete("selection")
        n=10*self.zoomFactor
        for i in self.selection:
            x=i.x*self.zoomFactor
            y=i.y*self.zoomFactor
            self.canevasEspace.create_rectangle(x-n,y-n,x+n,y+n,outline="yellow",dash=( 2, 2 ),  tags=("selection"))
        
        self.canevasEspace.delete("etoileselection")
        
        if self.etoileselection:
            n=10*self.zoomFactor
            i=self.etoileselection
            x=i.x*self.zoomFactor
            y=i.y*self.zoomFactor
            self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,outline="yellow",dash=( 2, 2 ),  tags=("etoileselection"))
        
        self.canevasSysteme.delete("planeteselection")
        if self.planeteselection :
            divx=self.canevasSysteme.winfo_width()/2
            divy=self.canevasSysteme.winfo_height()/2
            maxx=divx*2
            maxy=divy*2
            i=self.etoileselection
            for j in i.planetes:
                if abs(j.x)>maxx:
                    maxx=abs(j.x)
                if abs(j.y)>maxy:
                    maxy=abs(j.y)
            rapx=divx/maxx*0.95
            rapy=divy/maxy*0.95
            if rapx>rapy:
                rap=rapy
            else:
                rap=rapx
                i = self.planeteselection
                n=i.taille*3*rap +(i.taille*3*rap)/2
                x=i.x*rapx
                y=i.y*rapy
                self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="yellow",dash=( 2, 2 ),  tags=("planeteselection"))

        self.canevasPlanete.delete("travailleurselection")
        if self.travailleurselection :
            dif=10
            i = self.travailleurselection
            print("id",i.id)
            print("pos",i.x-dif,i.y-dif,i.x+dif,i.y+dif)
            self.canevasPlanete.create_oval(i.x-dif,i.y-dif,i.x+dif,i.y+dif,outline="yellow",dash=( 2, 2 ),  tags=("travailleurselection"))

                    
    def changeCible(self,selection,e):
        #s=input("WO")
        #print("CHANGE",e.x,e.y)

        x=e.x
        y=e.y
        x1 = self.canevasEspace.canvasx(x)
        y1 = self.canevasEspace.canvasy(y)
        for i in selection:
            self.parent.changeCible(i.id,x1,y1)
            
    def creerServeur(self):
        nom=self.nomjoueur.get()
        leip=self.parent.monip
        if nom:
            pid=self.parent.creerServeur()
            if pid:
                self.demarreB.config(state=NORMAL)
                self.root.after(500,self.inscritClient)
        else:
            mb.showerror(title="Besoin d'un nom",message="Vous devez inscrire un nom pour vous connecter.")
            
                
    def inscritClient(self):
        nom=self.nomjoueur.get()
        leip=self.parent.monip
        self.parent.inscritClient(nom,leip)
        
    def connecterServeur(self):
        nom=self.nomjoueur.get()
        leip=self.autreip.get()
        if nom:
            self.parent.inscritClient(nom,leip)
    
    def afficheModele(self,civs):
        if self.perspective=="cosmos":
            return
        elif self.perspective=="espace":
            self.canevasEspace.delete("vaisseau")
            for i in civs.keys():
                 for j in civs[i].artefacts["vaisseau"]:
                    if moi:
                        x1=0
                        self.canevas.create_rectangle(moi.x-5+x1,moi.y-5,moi.x+5+x1,moi.y+5,fill="blue",tags=("vaisseau",))
                    if autre:
                        for i in autre:
                            self.canevas.create_rectangle(i.x-5,i.y-5,i.x+5,i.y+5,fill="lightgreen",tags=("vaisseau",))
                            self.canevas.create_text(i.x-5,i.y+5,text=i.nom,tags=("vaisseau",))
                
    def afficheEtoiles(self):
        self.canevasEspace.delete("etoiles")
        self.canevasEspace.delete("pseudoetoiles")
        n=3
        for i in self.partie.etoiles:
            n=i.taille/20*self.zoomFactor
            x=i.x*self.zoomFactor
            y=i.y*self.zoomFactor
            print(x,y)
            self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,fill="yellow",tags=("etoiles",i.id))
        e=self.partie.civs[self.parent.nom].planeteMere.parent
        n=4*self.zoomFactor
        x=e.x*self.zoomFactor
        y=e.y*self.zoomFactor
        self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,width=2,outline="red",tags=("etoiles",e.id,self.parent.nom,"etoilemere"))
        n=1
        for i in self.partie.pseudoetoiles:
            x=i[0]*self.zoomFactor
            y=i[1]*self.zoomFactor
            self.canevasEspace.create_oval(x,y,x+n,y+n,fill="white",tags=("pseudoetoiles",))
        
    def showPlanete(self):
        if self.perspective=="espace":
            self.placeCadre(self.cadrePlanete)
            self.perspective="planete"
            self.showB.config(text="Show Espace")
        else:
            self.placeCadre(self.ecranPartie)
            self.perspective="espace"
            self.showB.config(text="Show Planete")
        
    def centrerPlanete(self):
        self.centrerObjet( self.partie.civs[self.parent.nom].planeteMere.parent)
        
    def centrerObjet( self, obj):
        x=obj.x
        y=obj.y
        sx = float(self.x_espace)
        ecranx=float( self.canevasEspace.winfo_width() )/2.0
        #print(ecranx)
        posx = ( x-ecranx )/sx
        self.canevasEspace.xview( "moveto", posx )
        
        sy = float( self.y_espace )
        ecrany=float( self.canevasEspace.winfo_height() )/2.0
        posy = ( y-ecrany )/sy
        self.canevasEspace.yview( "moveto", posy )
        
    def ecrireMessage(self,message,nom):
        self.chatText.config(state=NORMAL)
        self.chatText.insert(END, '\n')
        self.chatText.insert(END, nom+": "+message)
        self.chatText.yview(END)
        self.chatText.config(state=DISABLED)
        
    def envoyerMessage(self,event):
        message = self.inputText.get()
        if message == "/ip":
            self.ecrireIP()
        else:
            self.parent.envoyerMessage(message)
        self.inputText.delete(0,END)
    
    def ecrireIP(self):
        self.chatText.config(state=NORMAL)
        self.chatText.insert(END, '\n')
        self.chatText.insert(END, "IP est : "+self.parent.monip)
        self.chatText.yview(END)
        self.chatText.config(state=DISABLED)
        
    def construire(self,event):
        if self.travailleurselection:
            i=self.travailleurselection
            i.batimentConstruire = "Command Center"
            self.deplacer(event)
            
    def deplacer(self,event=None):  
        if self.travailleurselection:
            i=self.travailleurselection
            if event:
                i.cible.append(self.canevasPlanete.canvasx(event.x))
                i.cible.append(self.canevasPlanete.canvasy(event.y))
            if i.cible:
                i.DeplacerTravailleur(i.x,i.y,i.cible[0],i.cible[1])
                
                